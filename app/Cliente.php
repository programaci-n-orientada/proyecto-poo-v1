<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = [
        'nombres',
        'apellidos',
        'cedula',
        'direccion',
        'telefono',
        'fecha_nacimiento',
        'email'
        ];
}
