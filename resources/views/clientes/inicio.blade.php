@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">MODULO DE CLIENTES</div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Nombres</th>
                            <th scope="col">Apellidos</th>
                            <th scope="col">Cedula</th>
                            <th scope="col">Direcciòn</th>
                            <th scope="col">Telefono</th>
                            <th scope="col">Fecha nacimiento</th>
                            <th scope="col">Email</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($cliente as $item)   
                          <tr>
                            <th scope="row">{{$item->id}}</th>
                            <td>{{$item->nombres}}</td>
                            <td>{{$item->apellidos}}</td>
                            <td>{{$item->cedula}}</td>
                            <td>{{$item->direccion}}</td>
                            <td>{{$item->telefono}}</td>
                            <td>{{$item->fecha_nacimiento}}</td>
                            <td>{{$item->email}}</td>                          
                          </tr>  
                          @endforeach                        
                        </tbody>
                      </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
